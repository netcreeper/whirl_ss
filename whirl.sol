pragma solidity ^0.4.24;

import "./SmartToken.sol";

interface tokenRecipient { function receiveApproval(address _from, uint256 _value, address _token, bytes _extraData) external; }


contract WhirlToken is SmartToken {

    string public name = "Whirl";
    string public symbol = "WRL";
    uint8 public  decimals = 18;

    uint256 public constant INITIAL_SUPPLY = 60000000000 * (10 ** uint256(decimals));

    mapping (address => bool) public frozenAccount;

    /* This generates a public event on the blockchain that will notify clients */
    event FrozenFunds(address target, bool frozen);

    /**
   * @dev Constructor that gives msg.sender all of existing tokens.
   */
    constructor() 
        public 
        SmartToken(name, symbol, decimals)
    {
        issue(msg.sender, INITIAL_SUPPLY);
    }

    /// @notice `freeze? Prevent | Allow` `target` from sending & receiving tokens
    /// @param target Address to be frozen
    /// @param freeze either to freeze it or not
    function freezeAccount(address target, bool freeze) public ownerOnly  {
        frozenAccount[target] = freeze;
        emit FrozenFunds(target, freeze);
    }

    /* This unnamed function is called whenever someone tries to send ether to it */
    function() public {
        revert();
	// Prevents accidental sending of ether
    }
}